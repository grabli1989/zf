<?php
/**
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2016 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Application;

use Zend\Http\Request;
use Zend\I18n\Translator\Translator;
use Zend\ModuleManager\ModuleManager;
use Zend\Mvc\MvcEvent;

class Module
{
    const VERSION = '3.0.3-dev';
    private $serviceManager;

    public function getConfig()
    {
        return include __DIR__ . '/../config/module.config.php';
    }

    public function init(ModuleManager $manager)
    {
        // Get event manager.
        $eventManager = $manager->getEventManager();
        $sharedEventManager = $eventManager->getSharedManager();
        // Register the event listener method.
        $sharedEventManager->attach(__NAMESPACE__, 'dispatch',
            [$this, 'onDispatch'], 100);
    }

    public function onDispatch(MvcEvent $e) {
        /** @var Request $request */
        $request = $e->getApplication()->getServiceManager()->get('request');
        $lang = $request->getQuery('lang');
        if ($lang) {
            /** @var MvcEvent $e */
            $translator = $e->getApplication()->getServiceManager()->get(\Zend\I18n\Translator\TranslatorInterface::class);
            /** @var Translator $translator */
            $translator->setLocale($lang);
        }
    }
}
